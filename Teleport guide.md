[![](http://img.ipev.ru/2017/09/12/gitlab_cover.png)](http://teleport.media)

# About Teleport

Teleport - is the platform for decentralized delivery of video streaming content.

Using the platform can significantly reduce the load on your media server when you broadcast video content and improve the quality of the broadcasting.
This is due to the automatic deployment of the peer-to-peer network from browsers that broadcast the same video and exchange parts of this content between each other.

## How it works
Teleport consists of a server and an SDK. The solution allows browsers to connect to the p2p-network using WebRTC and switch the receiving source of each streaming chunk for each node between the media server and P2P. Teleport supports adaptive HTTP-based transport protocols only, such as HLS and DASH.

To use the Teleport SDK, you can use one of the proposed plugins. They take all the work on initializing the script and integrating it with a media player.

Plugins list:
* [Shaka Player ](https://gitlab.tlprt.cloud/teleport.media/shaka-player-p2p)
* [hls.js](https://gitlab.tlprt.cloud/teleport.media/p2p-hls.js)
* [Video.js HLS](https://gitlab.tlprt.cloud/teleport.media/video-js-p2p)

The list of plugins will be updated.

When initialized, the Teleport SDK establishes a connection to the tracker server. It is used for coordinating peer connections. Communication with the tracker server occurs via the WebSocket protocol using an encrypted connection. After connecting to the tracker server, the peer receives a message with unique peer IDs to establish a connection with them. After the connections are established, the peers exchange messages and make their own decisions on the exchange of segments. 

To perform resource-intensive operations, the Teleport.js client script uses Web Workers. Storage of received data in memory on user devices is organized in such a way that each piece of video, regardless of the source of receipt (P2P or server) is stored on the device, specified by the "TTL time" parameter (default TTL = 180 sec). After this time, the piece is removed from the device. Data transfer in the P2P network via the WebRTC channel occurs with the breakdown of the transmitted data into pieces of 16KB, their reassembly on the receiving side and integrity check (hash-sum from the content).

Teleport is compatible with any CAS / DRM system but requires additional integration with them.

# Integrating Teleport
## Domain confirmation
Sequencing:
1. Go through the signup process on https://cabinet.teleport.media
2. Proceed your domain confirmation in the "Integration" section.
3. After the domain is confirmed, in the "Domains" tab you'll get the Teleport API key for running the script.

Now you can connect Teleport in accordance with the documentation, and use the key given to you by the API.

## In-code changes
To integrate with Teleport, you need to install a JavaScript library 
```<script src="https://cdn.teleport.media/teleport.js"></script>``` 
and initialize it.

**When using ready-made plug-ins, initialization is not needed.**
 
**For correct work of the platform, it is necessary to connect teleport.js after connecting the library of the video player.**

Example of Teleport script installation:

```html
$ index.html
    <script src="//somecdn.com/shaka-player.compiled.js"></script>
    <script src="https://cdn.teleport.media/teleport.js"></script>
    <script src="https://cdn.teleport.media/shaka-player.teleport.js"></script>
```

Example of Teleport script initialization:
```html
$ index.html
    <script>
        window.teleport.init(
		{apiKey: 'TLPRT_API_KEY', // { string }  teleport.js api key. Get the TLPRT_API_KEY in your user area on cabnet.teleport.media 
      debug: true, // { boolean } 
      //Configuring the display of teleport.js system messages to the console, accessing global objects.
      // Teleport groups peers by reference to the manifest. 
      // In case if you're using different servers to distribute the manifest, 
      // place a function that turns the manifest from different URL addresses into the same form, into the value of the parameter
      manifestUrlFormatter: (manifestLink) => { //default value
        const url = new URL(manifestLink)

        return url.pathname
      },
	  
      // The same for the segments
      segmentIdFormatter: (segmentLink) => {  //default value
        const url = new URL(segmentLink)

        return url.pathname
      }
    }
)
    </script>
```

# API
## Events

To receive feedback from teleport.js in real time, you need to subscribe to the events generated in the window.teleport.events method. 

It takes the following parameters:

| Parameter | Description |
| ------------- | ------------- |
| **eventName** (string) | API event name  |
| **callback** (function)  | The function where the received result will be sent, after the event is completed.  |


*Example*
```html
$ skeleton
  <script>
    window.teleport.events.on('eventName', (...args) => {
        doSomeActions(...args)
    })
  </script>
$ To restrict an access to unauthorized media content users, initialize the script only after authorization.
```

#### Events list:

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **noWebrtc** | *number* **performance.now()** |The browser does not support WebRTC. |
*Example:*
```javascript
window.teleport.events.on('noWebrtc',(date) => console.log('change your IE', date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **peerConnected**| *string* **userId**, *number* **performance.now()** |A WebRTC connection has been established. userId - is the unique ID of the peer. |
*Example:*
```javascript
window.teleport.events.on('peerConnected', (userId, date) => console.log('New peer connected.', userId, date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **peerDisconnected**| *string* **userId**, *number* **performance.now()** |WebRTC connection is broken. |
*Example:*
```javascript
window.teleport.events.on('peerDisconnected', (userId, date) => console.log('peerDisconnected', userId, date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **statSegment**| *string* **targetId**('cdn'/ PeerTargetId),  *string* **segmentId**, *int* **byteLength**,  *int* **loadTime**, *number* **performance.now()**|Video file segment is loaded. If the segment file is loaded from CDN targetId === 'cdn'. |
*Example:*
```javascript
window.teleport.events.on('statSegment', (targetId, segmentId, byteLength, loadTime, date) => console.log('New segment loaded:', segmentId))
```

------------


#### Methods list:

| Event Name | Arguments |
| ------------- | ------------- |
| **window.teleport.api.getSegment(url:String, quality:number, callback?:(Promise<res>)=>void):Promise** | performs a segment request, where: 'url' -  is the segment URL, 'quality' - segment quality id. |
| **window.teleport.api.getSegmentIdByUrl(url: string):string** | returns a unique segment identifier by URL.|
| **window.teleport.api.abort(url: string, callback?:()=>void):void**|cancels a segment request, where 'url' - is the URL of the segment.|
| **window.teleport.api.buffering(): void**|informs the Teleport server about the buffering events.|
| **window.teleport.api.setUploadState(true/false): void**|switches On and Off the upload of the segments in the P2P. The status of the current state can be found in the uploadState property of the getStats method.|
|**window.teleport.api.segmentInfo(url/segmentId, callback?:()=>void): string**| returns the source of segment load ('cdn' / targetId / 'Segment not loaded')|

*Example:*
```javascript
$ javascript
    ...
    window.teleport.init(opts)
    window.teleport.segmentInfo("https://somecdn.com/hd/mediaSegment1.ts")
    window.teleport.segmentInfo("any segment information")
    window.teleport.segmentInfo("other any segment information")
$ console output example
    "cdn"
    "facfdb535ac9500c"
    "Segment not loaded"
```

------------


| Event Name | Arguments |
| ------------- | ------------- |
|**window.teleport.api.getStats (): Object**| returns aggregated statistics on the downloaded segments from the media server and from the P2P.|

*Example:*
```javascript
$ javascript
    ...
    window.teleport.api.getStats()
$ console output example
  {
    "uploadState": true,
    "metrics": {
      "peerCount": 2,
      "cdnTotalByteLength": 7748493,
      "cdnTotalLoadTime": 2073.6700000000005,
      "cdnDownloadCount": 5,
      "pdnTotalByteLength": 0,
      "pdnTotalLoadTime": 0,
      "pdnDownloadCount": 0,
      "uploadCount": 1,
      "cdnAvSpeed": 28.508060645921912,
      "pdnAvSpeed": 0
    },
    "peers": [
      {
        "segmentsCount": 7,
        "targetId": "d5ba30802f77e9e7",
        "accuracy": 0,
        "rating": 0
      },
      {
        "segmentsCount": 6,
        "targetId": "d5ba30805768c0f3",
        "accuracy": 16.666666666666664,
        "rating": 0
      }
    ]
  }

```
____________________________

# Teleport support

[![](http://img.ipev.ru/2017/09/12/slack.png)](https://join.slack.com/t/tlprt-support/shared_invite/MjM5ODU5MTg1NTczLTE1MDUyMTIxODktMDljMzRlZGQ1OQ)         [![](http://img.ipev.ru/2017/09/12/email3ef58.png)](mailto:support@teleport.media?subject=Teleport on Gitlab)
------------

[![](http://img.ipev.ru/2017/09/12/facebook.png)](https://www.facebook.com/teleportcdn/)
