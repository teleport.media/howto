[![](http://img.ipev.ru/2017/09/12/gitlab_cover.png)](http://teleport.media)

# О платформе Teleport

Teleport - это платформа распределенной доставки потокового видео.

Использование платформы позволяет значительно снизить нагрузку на ваш медиасервер при трансляции видеоконтента и повысить качество трансляции. 
Это происходит благодаря автоматической организации пиринговой сети из браузеров, которые транслируют одинаковое видео и обмениваются частями этого контента между ними. 

## Принцип работы
Teleport состоит из cервера и SDK. Решение позволяет подключать браузеры пользователей к пиринговой сети с помощью WebRTC и переключать источник получения каждого куска контента для каждого узла с медиасервера на пиринговую сеть и обратно. Teleport поддерживает только адаптивные транспортные протоколы видео, основанные на HTTP, такие как HLS и DASH. 

Для использования Teleport SDK  можно воспользоваться одним из готовых плагинов. Плагины берут на себя всю работу по инициализации скрипта и его интеграции с определенный медиаплеером. 

Список плагинов c документациями:
* [Shaka Player ](https://gitlab.tlprt.cloud/teleport.media/shaka-player-p2p)
* [hls.js](https://gitlab.tlprt.cloud/teleport.media/p2p-hls.js)
* [Video.js HLS](https://gitlab.tlprt.cloud/teleport.media/video-js-p2p)

Список плагинов будет пополняться.

При инициализации Teleport SDK устанавливает соединение c трекер-сервером. Он используется для координирования соединений пиров. Общение с трекер-сервером происходит по протоколу Websocket с использованием шифрованного соединения. После подключения к трекер-серверу пир получает сообщение с уникальными идентификаторами пиров для установки соединения с ними. После установки соединений пиры обмениваются сообщениями и самостоятельно принимают решение об обмене сегментами. 

Для выполнения ресурсоемких операций клиентский скрипт teleport.js использует Web Workers. Хранение полученных данных на устройствах пользователей организовано в памяти таким образом, что каждый кусок видео независимо от источника получения (пиринговая сеть или сервер) хранится на устройстве, заданное через параметр TTL время (по умолчанию TTL=180 сек). По истечении этого времени кусок удаляется с устройства. Передача данных в пиринговой сети через WebRTC-канал происходит с разбивкой передаваемых данных на куски размером 16КБ, их обратной сборкой на принимающей стороне и проверкой целостности (хеш-сумма от содержимого).

Teleport совместим с любыми CAS/DRM системами, но требует дополнительной интеграции с ними.

# Интеграция
## Валидация домена
Последовательность действий:
1. Необходимо зарегистрироваться на https://cabinet.teleport.media
2. В разделе "Интеграция" пройти процедуру валидации домена любым удобным из указанных способов..
3. После подтверждения домена, во вкладке "My domains" вы получите API ключи для работы скрипта.

Теперь вы можете начать подклчение Teleport с помошью дальнейших инстуркций.

## Добавление кода
Для интеграции Teleport необходимо установить JavaScript библиотеку 
```<script src="https://cdn.teleport.media/teleport.js"></script>``` и инициализировать её.  

**При использовании готовых плагинов инициализация не нужна.**
 
**Для корректной работы платформы необходимо подключить teleport.js после подключения библиотеки видео плеера.**

Пример установки скрипта:

```html
$ index.html
    <script src="//somecdn.com/shaka-player.compiled.js"></script>
    <script src="https://cdn.teleport.media/teleport.js"></script>
    <script src="https://cdn.teleport.media/shaka-player.teleport.js"></script>
```

Пример инициализации скрипта:
```html
$ index.html
    <script>
        window.teleport.init(
		{apiKey: 'TLPRT_API_KEY', // { string }  teleport.js api key. Можно получить в личном кабинете.
      debug: true, // { boolean } 
      //Настройка отображения системных сообщений teleport.js в консоль, доступ к глобальным объектам.
      // teleport группирует пиров по ссылке на манифест
      // В случае если вы используете разные сервера для раздачи манифеста,
      // передайте в значение параметра функцию, которая приводит ссылку на манифест 
      // с разных url адресов к одинаковому виду
      manifestUrlFormatter: (manifestLink) => { //default value
        const url = new URL(manifestLink)

        return url.pathname
      },
	  
      // Аналогично для сегментов
      segmentIdFormatter: (segmentLink) => {  //default value
        const url = new URL(segmentLink)

        return url.pathname
      }
    }
)
    </script>
```

# API
## События

Для получения обратной связи от teleport.js в режиме реального времени необходимо подписаться на события генерируемые в методе window.teleport.events. Он принимает следующие параметры:

| Параметр | Описание |
| ------------- | ------------- |
| **eventName** (string) | название события API  |
| **callback** (function)  | функция, в которую будет передан полученный результат после выполнения события. |

*Пример:*
```html
$ skeleton
  <script>
    window.teleport.events.on('eventName', (...args) => {
        doSomeActions(...args)
    })
  </script>
$ Для ограничения доступа к медиаконтенту неавторизированных пользователей инициализируйте скрипт 
только после авторизации.
```

#### Список событий:

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **noWebrtc** | *number* **performance.now()** |Браузер не поддерживает webrtc. |
*Пример:*
```javascript
window.teleport.events.on('noWebrtc',(date) => console.log('change your IE', date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **peerConnected**| *string* **userId**, *number* **performance.now()** |Установлено WebRTC соединение. userId - уникальный Id пира.|
*Пример:*
```javascript
window.teleport.events.on('peerConnected', (userId, date) => console.log('New peer connected.', userId, date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **peerDisconnected**| *string* **userId**, *number* **performance.now()** |WebRTC соединение разорвано. |
*Пример:*
```javascript
window.teleport.events.on('peerDisconnected', (userId, date) => console.log('peerDisconnected', userId, date))
```

------------

| Event Name | Arguments | Description |
| ------------- | ------------- | ------------- |
| **statSegment**| *string* **targetId**('cdn'/ PeerTargetId),  *string* **segmentId**, *int* **byteLength**,  *int* **loadTime**, *number* **performance.now()**|Загружен сегмент видео файла. Если сегмент загружен из CDN targetId === 'cdn'. |
*Пример:*
```javascript
window.teleport.events.on('statSegment', (targetId, segmentId, byteLength, loadTime, date) => console.log('New segment loaded:', segmentId))
```

#### Список методов:

| Event Name | Arguments |
| ------------- | ------------- |
| **window.teleport.api.getSegment(url:String, quality:number, callback?:(Promise<res>)=>void):Promise** |выполняет запрос сегмента. url - Url сегмента. quality - id качества сегмента. |
| **window.teleport.api.getSegmentIdByUrl(url: string):string** | возвращает уникальный идентификатора сегмента по URL. |
| **window.teleport.api.abort(url: string, callback?:()=>void):void**|отменяет запрос сегмента. url - URL сегмента.|
| **window.teleport.api.buffering(): void**|сообщает на сервер Teleport о событии буферизации.|
| **window.teleport.api.setUploadState(true/false): void**|включает/выключает отдачу сегментов в пиринг. Статус текущего состояния можно узнать в свойстве uploadState метода getStats.|
|**window.teleport.api.segmentInfo(url/segmentId, callback?:()=>void): string**| возвращает источник загрузки сегмента ('cdn' / targetId / 'Segment not loaded')|

*Пример:*
```javascript
$ javascript
    ...
    window.teleport.init(opts)
    window.teleport.segmentInfo("https://somecdn.com/hd/mediaSegment1.ts")
    window.teleport.segmentInfo("any segment information")
    window.teleport.segmentInfo("other any segment information")
$ console output example
    "cdn"
    "facfdb535ac9500c"
    "Segment not loaded"
```
------------


| Event Name | Arguments |
| ------------- | ------------- |
|**window.teleport.api.getStats (): Object**| отдает агрегированную статистику по загруженым сегментам с медиа сервера и из пиринговой сети.|
 
*Пример:*
```javascript
$ javascript
    ...
    window.teleport.api.getStats()
$ console output example
  {
    "uploadState": true,
    "metrics": {
      "peerCount": 2,
      "cdnTotalByteLength": 7748493,
      "cdnTotalLoadTime": 2073.6700000000005,
      "cdnDownloadCount": 5,
      "pdnTotalByteLength": 0,
      "pdnTotalLoadTime": 0,
      "pdnDownloadCount": 0,
      "uploadCount": 1,
      "cdnAvSpeed": 28.508060645921912,
      "pdnAvSpeed": 0
    },
    "peers": [
      {
        "segmentsCount": 7,
        "targetId": "d5ba30802f77e9e7",
        "accuracy": 0,
        "rating": 0
      },
      {
        "segmentsCount": 6,
        "targetId": "d5ba30805768c0f3",
        "accuracy": 16.666666666666664,
        "rating": 0
      }
    ]
  }

```
____________________________

# Teleport support

[![](http://img.ipev.ru/2017/09/12/slack.png)](https://join.slack.com/t/tlprt-support/shared_invite/MjM5ODU5MTg1NTczLTE1MDUyMTIxODktMDljMzRlZGQ1OQ)         [![](http://img.ipev.ru/2017/09/12/email3ef58.png)](mailto:support@teleport.media?subject=Teleport on Gitlab)
------------

[![](http://img.ipev.ru/2017/09/12/facebook.png)](https://www.facebook.com/teleportcdn/)
