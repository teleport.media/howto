### Ссылка на документацию
[Teleport JavaScript API References](https://docs.tlprt.cloud/ru/index.html)

### Поддерживаемые версии

Teleport.js совместим со всеми версиями Shaka Player старше 2.1.0

### Поддерживаемые форматы
Teleport.js протестирован с MPEG Dash. В скором времени ожидается поддержка HLS.js.

### Инициализация Teleport
Загрузка javascript модулей при интеграции Shaka Player с Teleport.js происходит строго в следующем порядке:

1. Shaka Player.
2. Teleport Core.
3. Teleport Shaka Player plugin.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/shaka-player/2.2.6/shaka-player.compiled.js"></script>
<script src="https://cdn.teleport.media/stable/teleport.js"></script>
<script src="https://cdn.teleport.media/stable/teleport.shaka.js"></script>
```


Полный пример инициализации для 1tv.ru:

```html
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TELEPORT SHAKA EXAMPLE</title>
    <script src="//cdnjs.cloudflare.com/ajax/libs/shaka-player/2.2.5/shaka-player.compiled.debug.js"></script>
</head>
<body>
<video id="video"
       width="640"
       muted
       poster="//shaka-player-demo.appspot.com/assets/poster.jpg"
       controls autoplay></video>
       
<!--Подключение основного SDK-->
<script src="https://cdn.teleport.media/stable/teleport.js"></script>
<!--Подключение плагина для Shaka Player-->
<script src="https://cdn.teleport.media/stable/teleport.shaka.1tv.js"></script>
<!--Скрипт инициализации-->
<script>
    let tlprt;
    const STREAM_URL = "<URL видео-манифеста>";
    const API_KEY = "<APIKEY полученных из кабинета>"
    function initApp()
    {
        shaka.polyfill.installAll();
        let video = document.getElementById("video");
        let player = new shaka.Player(video);
        player.addEventListener("error", onErrorEvent);
        
        //инициализируем экземпляр телепорт
        teleport.initialize({
                    apiKey: API_KEY,
                    loader: { // параметры интеграции с плеером
                        type: "shaka.1tv", //идентификатор плагина
                        params: {
                            player,
                            urlCleaner: (url) => (new URL(url)).pathname
                        } // экземпляр плеера
                    }
                })
                .then(instance =>
                {
                    //запускаем плеер и сохраняем экземпляр teleport
                    tlprt = instance;
                                        
                    return player.load(manifestUri);
                })
                .then(() =>
                {
                    // This runs if the asynchronous load is successful.
                    console.log('The video has now been loaded!');
                    // подписываемся за событие закрытия страницы, чтобы корректно освободить ресурсы
                    window.addEventListener("unload", () =>
                    {
                        if (tlprt)
                        {
                            tlprt.dispose();
                            tlprt = null;
                        }
                    });
                })
                .catch(onError);
    }

    function onErrorEvent(event)
    {
        // Extract the shaka.util.Error object from the event.
        onError(event.detail);
    }

    function onError(error)
    {
        // Log the error.
        console.error("Error code", error.code, "object", error);
    }

    document.addEventListener("DOMContentLoaded", initApp);
</script>
</body>
</html>
```